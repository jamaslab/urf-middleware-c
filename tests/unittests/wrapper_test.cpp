#include <gtest/gtest.h>

#include "urf/middleware_c/sockets/client.h"
#include "urf/middleware_c/sockets/server.h"

TEST(WrapperTests, client_create) {
    client_t* client = client_create("tcp://localhost:54321");
    ASSERT_TRUE(client != NULL);
    ASSERT_EQ(client_is_open(client), -1);
    ASSERT_EQ(client_open(client), -1);

    char* connection_strings[1] = {"tcp://*:54321"};

    server_t* server = server_create("server_test", connection_strings, 1);
    ASSERT_TRUE(server != NULL);
    ASSERT_EQ(server_is_open(server), -1);
    ASSERT_EQ(server_open(server), 0);
    ASSERT_EQ(client_open(client), 0);

    ASSERT_EQ(server_is_open(server), 0);
    ASSERT_EQ(client_is_open(client), 0);

    ASSERT_EQ(client_close(client), 0);
    ASSERT_EQ(server_close(server), 0);
}

TEST(WrapperTest, DISABLED_client_request) {
    client_t* client = client_create("tcp://localhost:8080");
    ASSERT_EQ(client_open(client), 0);
    auto msg = message_create(NULL, NULL);
    unsigned int responsesCount = 0;
    auto resp = client_request(client, msg, -1, responsesCount);
    
    ASSERT_EQ(responsesCount, 1);
}

TEST(WrapperTest, client_server_pull_timeout) {
     client_t* client = client_create("tcp://localhost:54321");
    ASSERT_TRUE(client != NULL);

    char* connection_strings[1] = {"tcp://*:54321"};

    server_t* server = server_create("server_test", connection_strings, 1);
    ASSERT_TRUE(server != NULL);
    ASSERT_EQ(server_open(server), 0);
    ASSERT_EQ(client_open(client), 0);

    auto retval = client_pull(client, 10);
    ASSERT_TRUE(retval == NULL);

    retval = server_pull(server, 10);
    ASSERT_TRUE(retval == NULL);
}

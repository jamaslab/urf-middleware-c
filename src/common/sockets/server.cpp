#include <stdlib.h>

#include "urf/middleware_c/sockets/server.h"
#include "common/messages/utils.h"

#include <urf/middleware/sockets/Server.hpp>

using urf::middleware::sockets::Server;
using urf::middleware::messages::Message;
using urf::middleware::messages::Body;
using urf::middleware::messages::Header;

struct server {
    void* obj;
};

server_t* server_create(char* service_name, char** connection_string, int connection_strings_count) {
    server_t *m;
    Server* obj;
    m = (server_t*)malloc(sizeof(*m));

    std::vector<std::string> connectionStrings;
    for (int i=0; i < connection_strings_count; i++) {
        connectionStrings.push_back(std::string(connection_string[i]));
    }

    try {
        obj = new Server(std::string(service_name), connectionStrings);
    } catch (const std::exception&) {
        return NULL;
    }
    
    obj->automaticallyDeserializeBody(false);
    m->obj = obj;

    return m;
}

void server_destroy(server_t* c) {
    if (c == NULL)
        return;
    delete static_cast<Server*>(c->obj);
    free(c);
}


int server_open(server_t* c) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);
    if (obj->open()) {
        return 0;
    }

    return -1;
}

int server_close(server_t* c) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);
    if (obj->close()) {
        return 0;
    }

    return -1;
}

int server_is_open(server_t* c) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);
    if (obj->isOpen()) {
        return 0;
    }

    return -1;
}

unsigned int server_connected_clients_count(server_t* s) {
    if (s == NULL)
        return -1;
    auto obj = static_cast<Server*>(s->obj);

    return static_cast<unsigned int>(obj->connectedClientsCount());
}

char* server_get_service_name(server_t* s) {
    if (s == NULL)
        return NULL;
    auto obj = static_cast<Server*>(s->obj);
    auto serviceName = obj->serviceName();
    return strcpy((char*)malloc(serviceName.length()+1), serviceName.c_str());
}

message_t* server_pull(server_t* c, long timeout) {
    if (c == NULL)
        return NULL;

    auto obj = static_cast<Server*>(c->obj);

    std::optional<Message> msg;

    if (timeout < 0) {
        msg = obj->pull();
    } else {
        msg = obj->pull(std::chrono::milliseconds(timeout));
    }

    if (!msg) return NULL;
    return message_create_from_obj(msg.value());
}

int server_push(server_t* c, message_t* message, unsigned int requires_ack) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);

    Message msg(*(static_cast<Body*>(message->body->obj)), *(static_cast<Header*>(message->header->obj)));

    if (obj->push(msg, requires_ack == 1 ? true : false)) {
        return 0;
    }

    return -1;
}

message_t** server_request(server_t* c, message_t* message, long timeout, unsigned int& responses_count) {
    if (c == NULL)
        return NULL;

    auto obj = static_cast<Server*>(c->obj);

    Message msg(*(static_cast<Body*>(message->body->obj)), *(static_cast<Header*>(message->header->obj)));
    std::vector<Message> responses;

    if (timeout < 0) {
        responses = obj->request(msg);
    } else {
        responses = obj->request(msg, std::chrono::milliseconds(timeout));
    }

    responses_count = responses.size();
    if (responses_count == 0) {
        return NULL;
    }
    message_t** responses_array = (message_t**)(malloc(responses_count*sizeof(message_t*)));
    for (unsigned int i=0; i < responses_count; i++) {
        responses_array[i] = message_create_from_obj(responses[i]);
    }    

    return responses_array;
}

message_t* server_receive_request(server_t* c, long timeout) {
    if (c == NULL)
        return NULL;

    auto obj = static_cast<Server*>(c->obj);

    std::optional<Message> msg;
    if (timeout < 0) {
        msg = obj->receiveRequest();
    } else {
        msg = obj->receiveRequest(std::chrono::milliseconds(timeout));
    }

    if (!msg)
        return NULL;
    return message_create_from_obj(msg.value());    
}

int server_respond(server_t* c, message_t* message) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);

    Message msg(*(static_cast<Body*>(message->body->obj)), *(static_cast<Header*>(message->header->obj)));
    if (obj->respond(msg)) {
        return 0;
    }

    return -1;
}

int server_subscribe(server_t* c, char* topic, unsigned int max_rate_ms) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);

    std::string topicStr(topic);

    if (obj->subscribe(topicStr, std::chrono::milliseconds(max_rate_ms))) {
        return 0;
    }

    return -1;
}

unsigned long server_subscription_count(server_t* c, char* topic) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);

    return static_cast<unsigned long>(obj->subscriptionsCount(std::string(topic)));
}

int server_unsubscribe(server_t* c, char* topic) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);

    std::string topicStr(topic);

    if (obj->unsubscribe(topicStr)) {
        return 0;
    }

    return -1;
}

int server_keep_update_history(server_t* c, unsigned int value) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);
    if (obj->keepUpdateHistory(value == 1 ? true : false)) {
        return 0;
    }

    return -1;
}

message_t* server_receive_update(server_t* c, char* topic, long timeout) {
    if (c == NULL)
        return NULL;

    auto obj = static_cast<Server*>(c->obj);

    std::optional<Message> msg;
    if (timeout < 0) {
        msg = obj->receiveUpdate(std::string(topic));
    } else {
        msg = obj->receiveUpdate(std::string(topic), std::chrono::milliseconds(timeout));
    }

    if (!msg)
        return NULL;
    return message_create_from_obj(msg.value());  
}

int server_publish(server_t* c, char* topic,  message_t* message, unsigned int requires_ack) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);

    Message msg(*(static_cast<Body*>(message->body->obj)), *(static_cast<Header*>(message->header->obj)));
    if (obj->publish(std::string(topic), msg, requires_ack == 1 ? true : false)) {
        return 0;
    }

    return -1;
}

char** server_available_partner_topics(server_t* c, unsigned int& output_length) {
    if (c == NULL)
        return NULL;

    auto obj = static_cast<Server*>(c->obj);

    auto topics = obj->availablePartnerTopics();

    output_length = topics.size();
    char** responses_array = (char**)(malloc(output_length*sizeof(char*)));
    for (unsigned int i=0; i < output_length; i++) {
        responses_array[i] = strcpy((char*)malloc(topics[i].length()+1), topics[i].c_str());
    }

    return responses_array;
}

int server_add_topic(server_t* c, char* topic) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);

    std::string topicStr(topic);

    if (obj->addTopic(topicStr)) {
        return 0;
    }

    return -1;
}

int server_remove_topic(server_t* c, char* topic) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Server*>(c->obj);

    std::string topicStr(topic);

    if (obj->removeTopic(topicStr)) {
        return 0;
    }

    return -1;
}

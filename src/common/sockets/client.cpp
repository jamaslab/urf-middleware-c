#include <stdlib.h>

#include "urf/middleware_c/sockets/client.h"
#include "common/messages/utils.h"

#include <urf/middleware/sockets/Client.hpp>

using urf::middleware::sockets::Client;
using urf::middleware::messages::Message;
using urf::middleware::messages::Body;
using urf::middleware::messages::Header;

struct client {
    void* obj;
};


client_t* client_create(char* connection_string) {
    client_t *m;
    Client *obj;
    m = (client_t*)malloc(sizeof(*m));
    try {
        obj = new Client(std::string(connection_string));
    } catch (const std::exception&) {
        return NULL;
    }

    obj->automaticallyDeserializeBody(false);

    m->obj = obj;

    return m;
}

void client_destroy(client_t* c) {
    if (c == NULL)
        return;
    delete static_cast<Client*>(c->obj);
    free(c);
}


int client_open(client_t* c) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);
    if (obj->open()) {
        return 0;
    }

    return -1;
}

int client_close(client_t* c) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);
    if (obj->close()) {
        return 0;
    }

    return -1;
}

int client_is_open(client_t* c) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);
    if (obj->isOpen()) {
        return 0;
    }

    return -1;
}

message_t* client_pull(client_t* c, long timeout) {
    if (c == NULL)
        return NULL;

    auto obj = static_cast<Client*>(c->obj);

    std::optional<Message> msg;

    if (timeout < 0) {
        msg = obj->pull();
    } else {
        msg = obj->pull(std::chrono::milliseconds(timeout));
    }

    if (!msg) return NULL;
    return message_create_from_obj(msg.value());
}

int client_push(client_t* c, message_t* message, unsigned int requires_ack) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);

    Message msg(*(static_cast<Body*>(message->body->obj)), *(static_cast<Header*>(message->header->obj)));

    if (obj->push(msg, requires_ack == 1 ? true : false)) {
        return 0;
    }

    return -1;
}

message_t** client_request(client_t* c, message_t* message, long timeout, unsigned int& responses_count) {
    if (c == NULL)
        return NULL;

    auto obj = static_cast<Client*>(c->obj);

    Message msg(*(static_cast<Body*>(message->body->obj)), *(static_cast<Header*>(message->header->obj)));
    std::vector<Message> responses;

    if (timeout < 0) {
        responses = obj->request(msg);
    } else {
        responses = obj->request(msg, std::chrono::milliseconds(timeout));
    }

    responses_count = responses.size();
    if (responses_count == 0) {
        return NULL;
    }

    message_t** responses_array = (message_t**)(malloc(responses_count*sizeof(message_t*)));
    for (unsigned int i=0; i < responses_count; i++) {
        responses_array[i] = message_create_from_obj(responses[i]);
    }    

    return responses_array;
}

message_t* client_receive_request(client_t* c, long timeout) {
    if (c == NULL)
        return NULL;

    auto obj = static_cast<Client*>(c->obj);

    std::optional<Message> msg;
    if (timeout < 0) {
        msg = obj->receiveRequest();
    } else {
        msg = obj->receiveRequest(std::chrono::milliseconds(timeout));
    }

    if (!msg)
        return NULL;
    return message_create_from_obj(msg.value());    
}

int client_respond(client_t* c, message_t* message) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);

    Message msg(*(static_cast<Body*>(message->body->obj)), *(static_cast<Header*>(message->header->obj)));
    if (obj->respond(msg)) {
        return 0;
    }

    return -1;
}

int client_subscribe(client_t* c, char* topic, unsigned int max_rate_ms) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);

    std::string topicStr(topic);

    if (obj->subscribe(topicStr, std::chrono::milliseconds(max_rate_ms))) {
        return 0;
    }

    return -1;
}

unsigned long client_subscription_count(client_t* c, char* topic) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);

    return static_cast<unsigned long>(obj->subscriptionsCount(std::string(topic)));
}

int client_unsubscribe(client_t* c, char* topic) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);

    std::string topicStr(topic);

    if (obj->unsubscribe(topicStr)) {
        return 0;
    }

    return -1;
}

int client_keep_update_history(client_t* c, unsigned int value) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);
    if (obj->keepUpdateHistory(value == 1 ? true : false)) {
        return 0;
    }

    return -1;
}

message_t* client_receive_update(client_t* c, char* topic, long timeout) {
    if (c == NULL)
        return NULL;

    auto obj = static_cast<Client*>(c->obj);

    std::optional<Message> msg;
    if (timeout < 0) {
        msg = obj->receiveUpdate(std::string(topic));
    } else {
        msg = obj->receiveUpdate(std::string(topic), std::chrono::milliseconds(timeout));
    }

    if (!msg)
        return NULL;
    return message_create_from_obj(msg.value());  
}

int client_publish(client_t* c, char* topic,  message_t* message, unsigned int requires_ack) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);

    Message msg(*(static_cast<Body*>(message->body->obj)), *(static_cast<Header*>(message->header->obj)));
    if (obj->publish(std::string(topic), msg, requires_ack == 1 ? true : false)) {
        return 0;
    }

    return -1;
}

// int client_on_update(client_t* c, char* topic, on_update_callback callback) {
//     if (c == NULL)
//         return NULL;

//     auto obj = static_cast<Client*>(c->obj);
//     std::string topic(topic);
// }


char** client_available_partner_topics(client_t* c, unsigned int& output_length) {
    if (c == NULL)
        return NULL;

    auto obj = static_cast<Client*>(c->obj);

    auto topics = obj->availablePartnerTopics();

    output_length = topics.size();
    char** responses_array = (char**)(malloc(output_length*sizeof(char*)));
    for (unsigned int i=0; i < output_length; i++) {
        responses_array[i] = strcpy((char*)malloc(topics[i].length()+1), topics[i].c_str());
    }

    return responses_array;
}

int client_add_topic(client_t* c, char* topic) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);

    std::string topicStr(topic);

    if (obj->addTopic(topicStr)) {
        return 0;
    }

    return -1;
}

int client_remove_topic(client_t* c, char* topic) {
    if (c == NULL)
        return -1;

    auto obj = static_cast<Client*>(c->obj);

    std::string topicStr(topic);

    if (obj->removeTopic(topicStr)) {
        return 0;
    }

    return -1;
}

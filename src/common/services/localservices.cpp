#include <stdlib.h>

#include "urf/middleware_c/services/localservices.h"

#include <urf/middleware/services/LocalServices.hpp>

using urf::middleware::services::LocalServices;

struct localservices {
    void* obj;
};

localservices_t* localservices_create() {
    localservices_t *m;
    LocalServices *obj;
    m = (localservices_t*)malloc(sizeof(*m));
    obj = new LocalServices();
    m->obj = obj;

    return m;
}

void localservices_destroy(localservices_t* lcs) {
    if (lcs == NULL)
        return;
    delete static_cast<LocalServices*>(lcs->obj);
    free(lcs);
}

int localservices_register_service(localservices_t* lcs, char* name, int length, char* details, int detailsLength) {
    if (lcs == NULL) {
        return -1;
    }

    auto obj = static_cast<LocalServices*>(lcs->obj);
    
    std::string serviceName(name, length);
    nlohmann::json detailsJson;
    if (detailsLength != 0) {
        detailsJson = nlohmann::json::parse(std::string(details, detailsLength), nullptr, false);
        if (detailsJson.is_discarded()) {
            return -1;
        }
    }

    if (obj->registerService(serviceName, detailsJson)) {
        return 0;
    }

    return -1;
}

int localservices_unregister_service(localservices_t* lcs, char* name, int length) {
    if (lcs == NULL) {
        return -1;
    }

    auto obj = static_cast<LocalServices*>(lcs->obj);
    std::string serviceName(name, length);

    if (obj->unregisterService(serviceName)) {
        return 0;
    }
    
    return -1;
}

int localservices_refresh_service(localservices_t* lcs, char* name, int length) {
    if (lcs == NULL) {
        return -1;
    }

    auto obj = static_cast<LocalServices*>(lcs->obj);
    std::string serviceName(name, length);

    if (obj->refreshService(serviceName)) {
        return 0;
    }
    
    return -1;
}

char* localservices_get_services(localservices_t* lcs) {
    if (lcs == NULL) {
        return NULL;
    }

    auto obj = static_cast<LocalServices*>(lcs->obj);
    auto services = obj->getServices();
    
    auto serializedServices = services.dump();

    char* servicesStr = new char[serializedServices.size()];
    std::copy(serializedServices.begin(), serializedServices.end(), servicesStr);

    return servicesStr;
}

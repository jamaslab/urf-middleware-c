#ifndef __UTILS_H__
#define __UTILS_H__

#include "urf/middleware_c/messages/header.h"
#include "urf/middleware_c/messages/message.h"

#include <urf/middleware/messages/Message.hpp>
#include <urf/middleware/messages/Header.hpp>

message_t* message_create_from_obj(urf::middleware::messages::Message& msg);
header_t* header_create_from_obj(urf::middleware::messages::Header& header);

#endif /* __UTILS_H__ */
#include <cstring>
#include <stdlib.h>

#include "urf/middleware_c/messages/message.h"

#include <urf/middleware/messages/Header.hpp>
#include <urf/middleware/messages/Body.hpp>
#include <urf/middleware/messages/Message.hpp>

using urf::middleware::messages::Header;
using urf::middleware::messages::Body;

message_t* message_create(body_t* body, header_t* header) {
    message_t *m;
    m = (message_t*)malloc(sizeof(*m));

    if (body == NULL) {
        m->body = body_create(NULL, 0);
    } else {
        m->body = body;
    }

    if (header == NULL) {
        m->header = body_get_header(m->body);
    } else {
        m->header = header;
    }

    return m;
}

void message_destroy(message_t* msg) {
    if (msg == NULL)
        return;

    body_destroy(msg->body);
    header_destroy(msg->header);
    free(msg);
}

body_t* message_get_body(message_t* msg) {
    if (msg == NULL)
        return NULL;

    return msg->body;
}

header_t* message_get_header(message_t* msg) {
    if (msg == NULL)
        return NULL;

    return msg->header;
}

void message_set_body(message_t* msg, body_t* body) {
    if (msg == NULL)
        return;

    msg->body = body;
}

void message_set_header(message_t* msg,  header_t* header) {
    if (msg == NULL)
        return;

    msg->header = header;
}

char* message_serialize(message_t* msg, long& length) {
    if (msg == NULL)
        return NULL;
    
    int header_length = 0;
    long body_length = 0;

    auto header_ptr = header_serialize(msg->header, header_length);
    // auto body_ptr = h

}

int message_deserialize(message_t* msg, char* bytes, long length) {
    return -1;
}

int message_set_bytes(message_t* msg, char* bytes, long length) {
    return -1;
}

int message_deserialize_set_bytes(message_t* msg) {
    return -1;
}

message_t* message_list_get_index(message_t** msg_list, unsigned int index) {
    return msg_list[index];
}

#include <cstring>
#include <stdlib.h>

#include "urf/middleware_c/messages/body.h"

#include <urf/middleware/messages/Header.hpp>
#include <urf/middleware/messages/Body.hpp>

using urf::middleware::messages::Header;
using urf::middleware::messages::Body;

body_t* body_create(char* data, long length) {
    body_t *m;

    nlohmann::json dataJson;
    if (data != NULL) {
        dataJson = nlohmann::json::parse(std::string(data, length), nullptr, false);
        if (dataJson.is_discarded()) {
            return NULL;
        }
    };

    Body *obj;
    m = (body_t*)malloc(sizeof(*m));
    obj = new Body(dataJson);
    m->obj = obj;

    return m;
}

void body_destroy(body_t* bdy) {
    if (bdy == NULL)
        return;
    delete static_cast<Body*>(bdy->obj);
    free(bdy);
}

char* body_serialize(body_t* bdy, long& length) {
    if (bdy == NULL) {
        return NULL;
    }

    auto obj = static_cast<Body*>(bdy->obj);
    auto bytes = obj->serialize();

    char* data = new char[bytes.size()];
    std::memcpy(data, bytes.data(), bytes.size());

    length = static_cast<long>(bytes.size());

    return data;
}

int body_deserialize(body_t* bdy, char* bytes, int length) {
    if (bdy == NULL) {
        return -1;
    }

    auto obj = static_cast<Body*>(bdy->obj);
    std::vector<uint8_t> bytesVector(bytes, bytes+length);
     if (obj->deserialize(bytesVector)) {
        return 0;
    }
    return -1;
}

int body_set_bytes(body_t* bdy, char* bytes, int length) {
    if (bdy == NULL) {
        return -1;
    }

    auto obj = static_cast<Body*>(bdy->obj);
    std::vector<uint8_t> bytesVector(bytes, bytes+length);
     if (obj->setBytes(std::move(bytesVector))) {
        return 0;
    }
    return -1;
}

int body_deserialize_set_bytes(body_t* bdy) {
    if (bdy == NULL) {
        return -1;
    }

    auto obj = static_cast<Body*>(bdy->obj);
    if (obj->deserialize()) {
        return 0;
    }
    return -1;
}

header_t* body_get_header(body_t* bdy) {
     if (bdy == NULL) {
        return NULL;
    }

    auto obj = static_cast<Body*>(bdy->obj);

    auto header = obj->getHeader();

    return header_create(header.length(), header.writerId(), header.timestamp(), header.isCompressed(), header.requiresAck());
}
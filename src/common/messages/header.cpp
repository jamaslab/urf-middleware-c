#include <cstring>
#include <stdlib.h>

#include "urf/middleware_c/messages/header.h"

#include <urf/middleware/messages/Header.hpp>

using urf::middleware::messages::Header;

header_t* header_create(unsigned long length, char writerId, unsigned long long timestamp, int is_compressed, int requires_ack) {
    header_t *m;
    Header *obj;
    m = (header_t*)malloc(sizeof(*m));
    obj = new Header(
        static_cast<uint32_t>(length),
        static_cast<uint8_t>(writerId),
        static_cast<uint64_t>(timestamp),
        static_cast<bool>(is_compressed),
        static_cast<bool>(requires_ack)
    );
    m->obj = obj;

    return m;
}

void header_destroy(header_t* hdr) {
    if (hdr == NULL)
        return;
    delete static_cast<Header*>(hdr->obj);
    free(hdr);
}

unsigned long header_get_header_length() {
    return Header::size;
}

int header_get_is_compressed(header_t* hdr) {
    if (hdr == NULL) {
        return -1;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    return static_cast<int>(obj->isCompressed());
}

int header_get_requires_ack(header_t* hdr) {
    if (hdr == NULL) {
        return -1;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    return static_cast<int>(obj->requiresAck());
}

unsigned long header_get_length(header_t* hdr) {
    if (hdr == NULL) {
        return 0;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    return static_cast<unsigned long>(obj->length());
}

char header_get_writer_id(header_t* hdr) {
    if (hdr == NULL) {
        return -1;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    return static_cast<char>(obj->writerId());
}

unsigned long long header_get_timestamp(header_t* hdr)  {
    if (hdr == NULL) {
        return 0;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    return static_cast<unsigned long long>(obj->timestamp());
}

char header_get_version(header_t* hdr) {
    if (hdr == NULL) {
        return -1;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    return static_cast<char>(obj->version());
}

unsigned long long header_get_delay(header_t* hdr) {
    if (hdr == NULL) {
        return 0;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    return static_cast<unsigned long long>(obj->delay());
}

void header_set_is_compressed(header_t* hdr, int is_compressed) {
    if (hdr == NULL) {
        return;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    obj->isCompressed(static_cast<bool>(is_compressed));
}

void header_set_requires_ack(header_t* hdr, int requires_ack) {
    if (hdr == NULL) {
        return;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    obj->requiresAck(static_cast<bool>(requires_ack));
}

void header_set_length(header_t* hdr, unsigned long length) {
    if (hdr == NULL) {
        return;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    obj->length(static_cast<uint32_t>(length));
}

void header_set_writer_id(header_t* hdr, char writer_id) {
    if (hdr == NULL) {
        return;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    obj->writerId(static_cast<uint8_t>(writer_id));
}

void header_set_timestamp(header_t* hdr, unsigned long long timestamp) {
    if (hdr == NULL) {
        return;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    obj->timestamp(static_cast<uint64_t>(timestamp));
}

void header_set_delay(header_t* hdr, unsigned long long delay) {
    if (hdr == NULL) {
        return;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    obj->delay(static_cast<uint64_t>(delay));
}

char* header_serialize(header_t* hdr, int& length) {
    if (hdr == NULL) {
        return NULL;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    auto bytes = obj->serialize();

    char* data = new char[bytes.size()];
    std::memcpy(data, bytes.data(), bytes.size());

    length = static_cast<int>(bytes.size());

    return data;
}


int header_deserialize_set_bytes(header_t* hdr) {
    if (hdr == NULL) {
        return -1;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    if (obj->deserialize()) {
        return 0;
    }
    return -1;
}

int header_deserialize(header_t* hdr, char* bytes, int length) {
    if (hdr == NULL) {
        return -1;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    std::vector<uint8_t> bytesVector(bytes, bytes+length);
     if (obj->deserialize(bytesVector)) {
        return 0;
    }
    return -1;
}

int header_set_bytes(header_t* hdr, char* bytes, int length) {
    if (hdr == NULL) {
        return -1;
    }

    auto obj = static_cast<Header*>(hdr->obj);
    std::vector<uint8_t> bytesVector(bytes, bytes+length);
     if (obj->setBytes(std::move(bytesVector))) {
        return 0;
    }
    return -1;
}


#include "common/messages/utils.h"

message_t* message_create_from_obj(urf::middleware::messages::Message& msg) {
    auto body_bytes = msg.body().serialize();
    auto body_ptr = body_create(NULL, 0);
    body_set_bytes(body_ptr, reinterpret_cast<char*>(body_bytes.data()),  body_bytes.size());
    return message_create(
        body_ptr,
        header_create_from_obj(msg.header())
    );
}

header_t* header_create_from_obj(urf::middleware::messages::Header& header) {
    auto header_c = header_create(header.length(), header.writerId(), header.timestamp(), header.isCompressed(), header.requiresAck());
    header_set_delay(header_c, header.delay());

    return header_c;
}

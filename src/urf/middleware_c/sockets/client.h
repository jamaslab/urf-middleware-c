#ifndef __CLIENT_H__
#define __CLIENT_H__

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware_c/urf_middleware_c_export.h"
#else
    #define URF_MIDDLEWARE_C_EXPORT
#endif

#include "urf/middleware_c/messages/message.h"

#ifdef __cplusplus
extern "C" {
#endif

URF_MIDDLEWARE_C_EXPORT struct client;
URF_MIDDLEWARE_C_EXPORT typedef struct client client_t;

URF_MIDDLEWARE_C_EXPORT client_t* client_create(char* connection_string);
URF_MIDDLEWARE_C_EXPORT void client_destroy(client_t* c);

URF_MIDDLEWARE_C_EXPORT int client_open(client_t* c);
URF_MIDDLEWARE_C_EXPORT int client_close(client_t* c);
URF_MIDDLEWARE_C_EXPORT int client_is_open(client_t* c);

URF_MIDDLEWARE_C_EXPORT message_t* client_pull(client_t* c, long timeout);
URF_MIDDLEWARE_C_EXPORT int client_push(client_t* c, message_t* message, unsigned int requires_ack);

URF_MIDDLEWARE_C_EXPORT message_t** client_request(client_t* c, message_t* message, long timeout, unsigned int& responses_count);
URF_MIDDLEWARE_C_EXPORT message_t* client_receive_request(client_t* c, long timeout);
URF_MIDDLEWARE_C_EXPORT int client_respond(client_t* c, message_t* message);

URF_MIDDLEWARE_C_EXPORT int client_subscribe(client_t* c, char* topic, unsigned int max_rate_ms);
URF_MIDDLEWARE_C_EXPORT unsigned long client_subscription_count(client_t* c, char* topic);
URF_MIDDLEWARE_C_EXPORT int client_unsubscribe(client_t* c, char* topic);

URF_MIDDLEWARE_C_EXPORT int client_keep_update_history(client_t* c, unsigned int value);
URF_MIDDLEWARE_C_EXPORT message_t* client_receive_update(client_t* c, char* topic, long timeout);

URF_MIDDLEWARE_C_EXPORT int client_publish(client_t* c, char* topic,  message_t* message, unsigned int requires_ack);
// URF_MIDDLEWARE_C_EXPORT int client_on_update(client_t* c, char* topic, on_update_callback callback);

URF_MIDDLEWARE_C_EXPORT char** client_available_partner_topics(client_t* c, unsigned int& output_length);

URF_MIDDLEWARE_C_EXPORT int client_add_topic(client_t* c, char* topic);
URF_MIDDLEWARE_C_EXPORT int client_remove_topic(client_t* c, char* topic);

#ifdef __cplusplus
}
#endif

#endif /* __CLIENT_H__ */
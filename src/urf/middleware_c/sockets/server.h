#ifndef __server_H__
#define __server_H__

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware_c/urf_middleware_c_export.h"
#else
    #define URF_MIDDLEWARE_C_EXPORT
#endif

#include "urf/middleware_c/messages/message.h"

#ifdef __cplusplus
extern "C" {
#endif

URF_MIDDLEWARE_C_EXPORT struct server;
URF_MIDDLEWARE_C_EXPORT typedef struct server server_t;

URF_MIDDLEWARE_C_EXPORT server_t* server_create(char* service_name, char** connection_string, int connection_strings_count);
URF_MIDDLEWARE_C_EXPORT void server_destroy(server_t* s);

URF_MIDDLEWARE_C_EXPORT int server_open(server_t* s);
URF_MIDDLEWARE_C_EXPORT int server_close(server_t* s);
URF_MIDDLEWARE_C_EXPORT int server_is_open(server_t* s);

URF_MIDDLEWARE_C_EXPORT unsigned int server_connected_clients_count(server_t* s);
URF_MIDDLEWARE_C_EXPORT char* server_get_service_name(server_t* s);

URF_MIDDLEWARE_C_EXPORT message_t* server_pull(server_t* s, long timeout);
URF_MIDDLEWARE_C_EXPORT int server_push(server_t* s, message_t* message, unsigned int requires_ack);

URF_MIDDLEWARE_C_EXPORT message_t** server_request(server_t* s, message_t* message, long timeout, unsigned int& responses_count);
URF_MIDDLEWARE_C_EXPORT message_t* server_receive_request(server_t* s, long timeout);
URF_MIDDLEWARE_C_EXPORT int server_respond(server_t* s, message_t* message);

URF_MIDDLEWARE_C_EXPORT int server_subscribe(server_t* s, char* topic, unsigned int max_rate_ms);
URF_MIDDLEWARE_C_EXPORT unsigned long server_subscription_count(server_t* s, char* topic);
URF_MIDDLEWARE_C_EXPORT int server_unsubscribe(server_t* s, char* topic);

URF_MIDDLEWARE_C_EXPORT int server_keep_update_history(server_t* s, unsigned int value);
URF_MIDDLEWARE_C_EXPORT message_t* server_receive_update(server_t* s, char* topic, long timeout);

URF_MIDDLEWARE_C_EXPORT int server_publish(server_t* s, char* topic,  message_t* message, unsigned int requires_ack);

URF_MIDDLEWARE_C_EXPORT char** server_available_partner_topics(server_t* s);

URF_MIDDLEWARE_C_EXPORT int server_add_topic(server_t* s, char* topic);
URF_MIDDLEWARE_C_EXPORT int server_remove_topic(server_t* s, char* topic);

#ifdef __cplusplus
}
#endif

#endif /* __server_H__ */
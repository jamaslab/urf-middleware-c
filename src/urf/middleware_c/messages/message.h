#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware_c/urf_middleware_c_export.h"
#else
    #define URF_MIDDLEWARE_C_EXPORT
#endif

#include "urf/middleware_c/messages/body.h"
#include "urf/middleware_c/messages/header.h"

#ifdef __cplusplus
extern "C" {
#endif

URF_MIDDLEWARE_C_EXPORT struct message {
    body_t* body;
    header_t* header;
};

URF_MIDDLEWARE_C_EXPORT typedef struct message message_t;

URF_MIDDLEWARE_C_EXPORT message_t* message_create(body_t* body, header_t* header);
URF_MIDDLEWARE_C_EXPORT void message_destroy(message_t* msg);

URF_MIDDLEWARE_C_EXPORT body_t* message_get_body(message_t* msg);
URF_MIDDLEWARE_C_EXPORT header_t* message_get_header(message_t* msg);

URF_MIDDLEWARE_C_EXPORT void message_set_body(message_t* msg, body_t* body);
URF_MIDDLEWARE_C_EXPORT void message_set_header(message_t* msg,  header_t* header);

URF_MIDDLEWARE_C_EXPORT char* message_serialize(message_t* msg, long& length);
URF_MIDDLEWARE_C_EXPORT int message_deserialize(message_t* msg, char* bytes, long length);

URF_MIDDLEWARE_C_EXPORT int message_set_bytes(message_t* msg, char* bytes, long length);
URF_MIDDLEWARE_C_EXPORT int message_deserialize_set_bytes(message_t* msg);

URF_MIDDLEWARE_C_EXPORT message_t* message_list_get_index(message_t** msg_list, unsigned int index);

#ifdef __cplusplus
}
#endif

#endif /* __MESSAGE_H__ */
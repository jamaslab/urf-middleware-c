#ifndef __HEADER_H__
#define __HEADER_H__

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware_c/urf_middleware_c_export.h"
#else
    #define URF_MIDDLEWARE_C_EXPORT
#endif

#ifdef __cplusplus
extern "C" {
#endif

URF_MIDDLEWARE_C_EXPORT struct header {
    void* obj;
};
URF_MIDDLEWARE_C_EXPORT typedef struct header header_t;

URF_MIDDLEWARE_C_EXPORT header_t* header_create(unsigned long length, char writerId, unsigned long long timestamp, int is_compressed, int requires_ack);
URF_MIDDLEWARE_C_EXPORT void header_destroy(header_t* hdr);

URF_MIDDLEWARE_C_EXPORT unsigned long header_get_header_length();

URF_MIDDLEWARE_C_EXPORT int header_get_is_compressed(header_t* hdr);
URF_MIDDLEWARE_C_EXPORT int header_get_requires_ack(header_t* hdr);
URF_MIDDLEWARE_C_EXPORT unsigned long header_get_length(header_t* hdr);
URF_MIDDLEWARE_C_EXPORT char header_get_writer_id(header_t* hdr);
URF_MIDDLEWARE_C_EXPORT unsigned long long header_get_timestamp(header_t* hdr);
URF_MIDDLEWARE_C_EXPORT char header_get_version(header_t* hdr);
URF_MIDDLEWARE_C_EXPORT unsigned long long header_get_delay(header_t* hdr);

URF_MIDDLEWARE_C_EXPORT void header_set_is_compressed(header_t* hdr, int is_compressed);
URF_MIDDLEWARE_C_EXPORT void header_set_requires_ack(header_t* hdr, int requires_ack);
URF_MIDDLEWARE_C_EXPORT void header_set_length(header_t* hdr, unsigned long length);
URF_MIDDLEWARE_C_EXPORT void header_set_writer_id(header_t* hdr, char writer_id);
URF_MIDDLEWARE_C_EXPORT void header_set_timestamp(header_t* hdr, unsigned long long timestamp);
URF_MIDDLEWARE_C_EXPORT void header_set_delay(header_t* hdr, unsigned long long delat);

URF_MIDDLEWARE_C_EXPORT char* header_serialize(header_t* hdr, int& length);
URF_MIDDLEWARE_C_EXPORT int header_deserialize(header_t* hdr, char* bytes, int length);
URF_MIDDLEWARE_C_EXPORT int header_set_bytes(header_t* hdr, char* bytes, int length);
URF_MIDDLEWARE_C_EXPORT int header_deserialize_set_bytes(header_t* hdr);

#ifdef __cplusplus
}
#endif

#endif /* __HEADER_H__ */
#ifndef __BODY_H__
#define __BODY_H__

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware_c/urf_middleware_c_export.h"
#else
    #define URF_MIDDLEWARE_C_EXPORT
#endif

#include "urf/middleware_c/messages/header.h"

#ifdef __cplusplus
extern "C" {
#endif

URF_MIDDLEWARE_C_EXPORT struct body {
    void* obj;
};
URF_MIDDLEWARE_C_EXPORT typedef struct body body_t;

URF_MIDDLEWARE_C_EXPORT body_t* body_create(char* data, long length);
URF_MIDDLEWARE_C_EXPORT void body_destroy(body_t* bdy);

URF_MIDDLEWARE_C_EXPORT char* body_serialize(body_t* bdy, long& length);
URF_MIDDLEWARE_C_EXPORT int body_deserialize(body_t* bdy, char* bytes, int length);

URF_MIDDLEWARE_C_EXPORT int body_set_bytes(body_t* bdy, char* bytes, int length);
URF_MIDDLEWARE_C_EXPORT int body_deserialize_set_bytes(body_t* bdy);

URF_MIDDLEWARE_C_EXPORT header_t* body_get_header(body_t* bdy);

#ifdef __cplusplus
}
#endif

#endif /* __BODY_H__ */
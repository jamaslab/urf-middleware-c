#ifndef __LOCALSERVICES_H__
#define __LOCALSERVICES_H__

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware_c/urf_middleware_c_export.h"
#else
    #define URF_MIDDLEWARE_C_EXPORT
#endif

#ifdef __cplusplus
extern "C" {
#endif

URF_MIDDLEWARE_C_EXPORT struct localservices;
URF_MIDDLEWARE_C_EXPORT typedef struct localservices localservices_t;

URF_MIDDLEWARE_C_EXPORT localservices_t* localservices_create();
URF_MIDDLEWARE_C_EXPORT void localservices_destroy(localservices_t* lcs);

URF_MIDDLEWARE_C_EXPORT int localservices_register_service(localservices_t* lcs, char* name, int length, char* details, int detailsLength);
URF_MIDDLEWARE_C_EXPORT int localservices_unregister_service(localservices_t* lcs, char* name, int length);

URF_MIDDLEWARE_C_EXPORT int localservices_refresh_service(localservices_t* lcs, char* name, int length);

URF_MIDDLEWARE_C_EXPORT char* localservices_get_services(localservices_t* lcs);

#ifdef __cplusplus
}
#endif

#endif /* __LOCALSERVICES_H__ */
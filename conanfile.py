import os
from conans import ConanFile, CMake, tools


def get_file(filename):
    f = open(filename, "r")
    return f.read()


class UrfMiddlewareCConan(ConanFile):
    _windows_import_paths = [
        "../Windows/{cf.settings.build_type}/bin/{cf.settings.build_type}"]

    name = "urf_middleware_c"
    version = "0.10.0"
    license = "MIT"
    author = "Giacomo Lunghi"
    url = "https://github.com/Jamaslab/urf_middleware_c"
    description = "Unified Robotic Framework Middleware C Wrapper"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "build_nuget": [True, False], "build_tests": [True, False]}
    import_paths = []
    short_paths = True
    default_options = {"shared": True, "build_nuget": False, "build_tests": False}
    requires = ("urf_middleware_cpp/0.10.0@uji-cirtesu-irslab+urobf+urf-middleware-cpp/stable")
    build_requires = ("cmake/3.25.0")
    generators = "cmake", "cmake_find_package", "virtualenv"
    exports_sources = ["environment/*", "src/*", "tests/*",
                       "CMakeLists.txt", "LICENSE", "README.md"]

    @property
    def default_user(self):
        return "uji-cirtesu-irslab+urobf+urf-middleware-c"

    @property
    def default_channel(self):
        return "stable"

    def requirements(self):
        pass

    def build_requirements(self):
        if self.options.build_tests:
            self.build_requires("gtest/1.10.0")

    def build(self):
        self.options["urf_middleware_cpp"].shared = True
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(
            self.recipe_folder, 'package/')
        cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace(
            "\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions["BUILD_TESTS"] = self.options.build_tests

        cmake.configure(
            build_folder='build/%s' % str(self.settings.os) + '/' + str(self.settings.build_type))
        cmake.build()
        if self.options.build_tests:
            cmake.test(output_on_failure=True)
        cmake.install()

        if self.options.build_nuget:
            self._create_nuget_package()

    def imports(self):
        # importing the dll where executable is for Visual Studio debugging - insted of use activate.bat script
        # FIXME : Temporary enable without any action to debug from Visual Studio
        # TODO : Use a vs plugins or a proper solution instead of copying dll
        if tools.os_info.is_windows:
            import_paths = getattr(self, 'import_paths') + \
                self._windows_import_paths

            for ipath in self.import_paths:
                self.copy("*.dll", src="lib", dst=ipath.format(cf=self))
                self.copy("*.pdb", src="lib", dst=ipath.format(cf=self))

    def package(self):
        self.copy("*.hpp", dst=".", src=os.path.join(self.recipe_folder,
                  'package/'), keep_path=True)
        self.copy("*.h", dst=".", src=os.path.join(self.recipe_folder,
                  'package/'), keep_path=True)
        if tools.os_info.is_linux:
            self.copy(
                "*.so", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
            self.copy(
                "*.a", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
        elif tools.os_info.is_windows:
            self.copy(
                "*.dll", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
            self.copy(
                "*.lib", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)

    def package_info(self):
        self.cpp_info.libs = ['urf_middleware_c']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib']

        self.env_info.PATH.append(os.path.join(self.package_folder, "lib"))

    def _create_nuget_package(self):
        properties = {
            "packageName": "Urf.Middleware.Native",
            "version": self.version,
            "author": self.author,
            "description": self.description
        }

        with open(os.path.join(self.recipe_folder, 'environment/nuget/TemplateCWrapper.csproj'), 'r') as templateFile:
            template = templateFile.read()

            with open(os.path.join(self.recipe_folder, f"package/{properties['packageName']}.csproj"), 'w+') as csProjFile:
                csProjFile.write(template)

        with open(os.path.join(self.recipe_folder, 'environment/nuget/TemplateCWrapper.nuspec'), 'r') as templateFile:
            template = templateFile.read()

            with open(os.path.join(self.recipe_folder, f"package/{self.name}.nuspec"), 'w+') as nuspecFile:
                nuspecFile.write(template.format(**properties))

        dlls = [f for f in os.listdir(os.path.join(
            self.recipe_folder, "package/lib")) if f.endswith('.dll')]

        itemsXml = ''
        for dll in dlls:
            itemsXml = itemsXml + f"\t\t<None Include=\"$(MSBuildThisFileDirectory)/../runtimes/win/native/{dll}\">\n\t\t\t<Link>{dll}</Link>\n\t\t</None>\n"

        with open(os.path.join(self.recipe_folder, 'environment/nuget/TemplateCWrapper.targets'), 'r') as templateFile:
            template = templateFile.read()
            with open(os.path.join(self.recipe_folder, f"package/{properties['packageName']}.targets"), 'w+') as targetsFile:
                targetsFile.write(template.format(itemsXml))

        self.run(f"cd {os.path.join(self.recipe_folder, 'package')} && dotnet pack {properties['packageName']}.csproj -p:NuspecFile={self.name}.nuspec --output nupkgs")
